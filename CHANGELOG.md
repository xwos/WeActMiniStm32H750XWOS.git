# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - beta

### Added

- 建立STM32CubeIDE的工程。
- Lua-5.4.6
- FatFS-0.15
- BSP
  - QSPI Flash
  - UART
  - SPI
  - SPI Flash
  - SPI LCD
  - SDCard
- libc
- Rust Runtime
- C++ Runtime
- 电源管理
